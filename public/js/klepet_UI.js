var swearArray = [];

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  var wordsTMP = [];
  var stevec = 0;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    sporocilo = sporocilo.replace(/</g, '&lt;');
    sporocilo = sporocilo.replace(/>/g, '&gt;');

    for(var i=0; i<swearArray.length; i++){
      var zvezdice = "";
      for(var j=0; j<swearArray[i].length-4; j++){
        zvezdice += "*";
      }
      sporocilo = sporocilo.replace(new RegExp(swearArray[i], "g"), zvezdice);
    }
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    sporocilo = sporocilo.replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">');
    sporocilo = sporocilo.replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">');
    sporocilo = sporocilo.replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">');
    sporocilo = sporocilo.replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">');
    sporocilo = sporocilo.replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">');
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var nickname = "";
  var channel = "";
  
    socket.on('swearArray', function(array) {
      swearArray = array.swearArray;
      for(var i = 0; i < swearArray.length; i++){
        swearArray[i]= "\\b".concat(swearArray[i], "\\b");
      }
    });

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      nickname = rezultat.vzdevek;
      var nicknamechannel = nickname.concat(" @ ",channel);
      $('#kanal').text(nicknamechannel);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    channel = rezultat.kanal;
    var nicknamechannel = nickname.concat(" @ ",channel);
    $('#kanal').text(nicknamechannel);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var besedilo = sporocilo.besedilo;
    
    besedilo = besedilo.replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">');
    besedilo = besedilo.replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">');
    besedilo = besedilo.replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">');
    besedilo = besedilo.replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">');
    besedilo = besedilo.replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">');
    var novElement = $('<div style="font-weight: bold"></div>').html(besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});